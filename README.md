﻿# Redmine Wiki Bundle

This Bundle handles the connection between the Redmine Wiki and your project.


## Installation

Make sure Composer is installed globally, as explained in the
[installation chapter](https://getcomposer.org/doc/00-intro.md)
of the Composer documentation.

### Application that use Symfony Flex

Open a command console, enter your project directory and execute:

```console
$ composer require mantic-digital/redmine-wiki
```

### Application that don't use Symfony Flex

#### Step 1: Download the bundle

Open a command console, enter your project directory and execute:

```console
$ composer require mantic-digital/redmine-wiki
```

#### Step 2: Enable the Bundle

Then, enable the bundle by adding it to the list of registered bundles
in the `config/bundles.php` file of your project:

```php
// config/bundles.php

return [
    // ...
    Mantic\RedmineWikiBundle\ManticRedmineWikiBundle::class => ['all' => true],
];
```

#### Step 3: Configure the Bundle

See [documentation](Resources/doc/index.md)