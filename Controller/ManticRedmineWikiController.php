<?php

/**
 * Copyright (c) 2020.. - Mantic
 */

namespace Mantic\RedmineWikiBundle\Controller;

use Mantic\RedmineWikiBundle\Services\RedmineService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class ManticRedmineWikiController extends AbstractController
{

    /** @var RedmineService $redmineService */
    private $redmineService;

    /**
     * ManticRedmineWikiController constructor.
     * @param RedmineService $redmineService
     */
    public function __construct(RedmineService $redmineService)
    {
        $this->redmineService = $redmineService;
    }

    /**
     * @param string $wikiRoute
     * @return Response
     */
    public function indexAction(string $wikiRoute='')
    {
        $wikiItems = $this->redmineService->call( $wikiRoute, true);
        // Generating breadcrumb
        $breadcrumb = $this->redmineService->generateBreadcrumb($wikiItems);

        return $this->render($this->redmineService->getView(), [
            'subtitle'    => '',
            'page'        => $this->redmineService->getHtmlFromXml($wikiItems),
            'attachments' => $this->redmineService->getAttachments($wikiItems),
            'breadcrumb'  => $breadcrumb
        ]);
    }

    /**
     * @param int $id
     * @return Response
     */
    public function downloadAttachmentAction(int $id)
    {
        $attachment = $this->redmineService->getAttachment($id, true);

        $response = new Response($attachment['content']);

        $disposition = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $attachment['name']
        );
        $response->headers->set('Content-Disposition', $disposition);

        return $response;
    }
}
