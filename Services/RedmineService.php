<?php

/**
 * Copyright (c) 2020.. - Mantic
 */

namespace Mantic\RedmineWikiBundle\Services;

use Mantic\RedmineWikiBundle\Helper\StringHelper;
use SimpleXMLElement;
use Symfony\Component\Routing\Router;
use Symfony\Component\Yaml\Yaml;

class RedmineService
{

    /** @var string $view */
    protected $view;

    /** @var string $entry_point */
    protected $entry_point;

    /** @var string $api_key */
    protected $api_key;

    /** @var string $base_url */
    protected $base_url;

    /** @var string $url */
    protected $url;

    /** @var string $attachments_path */
    protected $attachments_path;

    /** @var RouterInterface $router */
    protected $router;

    /** @var resource */
    protected $context;

    /**
     * RedmineService constructor.
     * @param Router $router
     * @param $view
     * @param $entry_point
     * @param $api_key
     * @param $base_url
     * @param $wiki_url
     * @param $attachments_path
     */
    public function __construct(Router $router, string $view, string $entry_point, string $api_key, string $base_url, string $wiki_url, string $attachments_path)
    {

        $this->view = $view;
        $this->entry_point = $entry_point;
        $this->api_key = $api_key;
        $this->base_url = $base_url;
        $this->url = $base_url . $wiki_url;
        $this->attachments_path = $attachments_path;

        $this->router = $router;

    }

    /**
     * @return string
     */
    public function getView()
    {
        if(!$this->view){
            throw new \RuntimeException('You forgot to indicate which view to use');
        }

        return $this->view;
    }

    /**
     * @return resource
     */
    protected function getContext()
    {
        if (null === $this->context) {
            // use key 'http' even if you send the request to https://...
            $options = [
                'http' => [
                    'header'  => "X-Redmine-API-Key: " . $this->api_key,
                    'method'  => 'GET'
                ]
            ];

            $this->context = stream_context_create($options);
        }

        return $this->context;
    }

    /**
     * @param string $route
     * @param bool $withAttachments
     * @return SimpleXMLElement
     */
    public function call(string $route, bool $withAttachments=false)
    {
        // Generating current wiki page
        $route = rtrim($route ? $route : $this->entry_point, "/");

        $url = $this->url . $route . '.xml';

        if ($withAttachments) {
            $url .= '?include=attachments';
        }

        $result = file_get_contents($url, false, $this->getContext());

        return new SimpleXMLElement($result);
    }

    /**
     * @param string $page
     * @return string
     */
    protected function getWikiPageUrl(string $page='')
    {
        $name       = 'mantic_redmine_wiki_homepage';
        $parameters = [];
        if (!empty($page)) {
            $page                    = StringHelper::title2Url($page);
            $name                    = 'mantic_redmine_wiki_page';
            $parameters['wikiRoute'] = $page;
        }

        return $this->router->generate($name, $parameters);
    }

    /**
     * @param string $page
     * @return array
     */
    public function generateBreadcrumb(SimpleXMLElement $page)
    {
        $wikiPages = $this->call('index');

        $hierarchy = [];

        foreach ($wikiPages as $wikiPage) {
            $hierarchy[(string)$wikiPage->title] = $wikiPage;
        }

        $path       = (string)$page->title;
        $continue   = $path !== '';
        $breadcrumb = [];

        do {
            if (!isset($hierarchy[$path])) {
                break;
            }

            $page = $this->call((string)$hierarchy[$path]->title);

            $content = $this->getHtmlFromXml($page);
            array_unshift($breadcrumb, ['title' => $content['title'], 'link' => $this->getWikiPageUrl($path)]);

            if (!$page->parent) {
                $continue = false;
            } else {
                $path = (string)$page->parent['title'];
            }
        } while ($continue);

        return $breadcrumb;
    }

    /**
     * @param SimpleXMLElement $wikiPages
     * @return array
     */
    public function getHtmlFromXml(SimpleXMLElement $wikiPages)
    {
        $result = '';
        $text   = $wikiPages->text;

        if (!empty($text)) {
            // Replacement of internal links
            if (preg_match_all('/\[\[([^\[\]]*)\]\]/', $text, $matches)) {
                foreach ($matches[1] as $match => $capture) {
                    if (false !== strpos($capture, '|')) {
                        list($url, $title) = explode('|', $capture);
                    } else {
                        $url = $title = $capture;
                    }

                    $url = $this->getWikiPageUrl($url);

                    $text = str_replace(
                        $matches[0][$match],
                        '<a href="'.$url.'">'.$title.'</a>',
                        $text
                    );
                }
            }

            // Replacement of strike text
            $text = preg_replace('/~~(.*)~~/', '<s>${1}</s>', $text);
            $text = str_replace('src="', 'src="'.$this->base_url, $text);

            $result = $text;
        }

        if (!empty($wikiPages->wiki_page)) {
            $result .= '<ul>';
            foreach ($wikiPages->wiki_page as $page) {
                $result .= '<li><a href="'.$this->getWikiPageUrl($page->title).'">' . $page->title . '</a></li>';
            }
            $result .= '</ul>';
        }

        // extract title
        return $this->extractTitle($result);
    }

    /**
     * @param string $content
     * @return array
     */
    protected function extractTitle(string $content)
    {
        if (preg_match('/<h1>(.*)<\/h1>/', $content, $matches)) {
            return [
                'title'     => $matches[1],
                'content'   => str_replace($matches[0], '', $content)
            ];
        }

        return [
            'title'     => '',
            'content'   => $content
        ];
    }

    /**
     * @param string $type
     * @return string
     */
    protected function getAttachmentType(string $type)
    {
        // must return the end of the class (after fa)
        switch (true) {
            case false !== strpos($type, 'pdf'):
                return '-pdf-o';

            case false !== strpos($type, 'image'):
                return '-image-o';

            default:
                return '';
        }
    }

    /**
     * @param SimpleXMLElement $wikiPage
     * @return array
     */
    public function getAttachments(SimpleXMLElement $wikiPage)
    {
        if (empty($wikiPage->attachments)) {
            return [];
        }

        $attachments = [];
        foreach ($wikiPage->attachments->attachment as $attachment) {
            $attachments[] = [
                'id'    => $attachment->id,
                'name'  => $attachment->filename,
                'desc'  => $attachment->description,
                'type'  => $this->getAttachmentType($attachment->content_type)
            ];
        }

        return $attachments;
    }

    /**
     * @param int $id
     * @param bool $download
     * @return array|SimpleXMLElement
     */
    public function getAttachment(int $id, bool $download=false)
    {
        $xml        = file_get_contents($this->base_url . $this->attachments_path . $id . '.xml', false, $this->getContext());
        $attachment = new SimpleXMLElement($xml);

        if (!$download) {
            return $attachment;
        }

        return [
            'name'      => $attachment->filename,
            'content'   => file_get_contents($attachment->content_url, false, $this->getContext())
        ];
    }
}
