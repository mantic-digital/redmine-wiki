<?php

/**
 * Copyright (c) 2020.. - Mantic
 */

namespace Mantic\RedmineWikiBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * @return TreeBuilder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('mantic_redmine_wiki');

        $treeBuilder->getRootNode()
                                ->children()
                                    ->scalarNode('view')
                                        ->info('Path file to the view to be used')
                                        ->cannotBeEmpty()->end()
                                    ->scalarNode('entry_point')
                                        ->info('Entry point for the wiki')
                                        ->defaultValue('wiki')->end()
                                    ->scalarNode('api_key')
                                        ->info('Project api key')->end()
                                    ->scalarNode('project_url')
                                        ->info('Url for the wiki project')->end()
                                    ->scalarNode('wiki_url')
                                        ->info('Path to the wiki page')
                                        ->end()
                                    ->scalarNode('attachments_path')
                                        ->info('Attachments path')
                                        ->defaultValue('attachments/')->end()
                            ->end()
                        ->end()
        ;

        return $treeBuilder;
    }
}