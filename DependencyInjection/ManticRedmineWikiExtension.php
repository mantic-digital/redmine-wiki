<?php

/**
 * Copyright (c) 2020.. - Mantic
 */

namespace Mantic\RedmineWikiBundle\DependencyInjection;


use Mantic\RedmineWikiBundle\Services\RedmineService;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Alias;

class ManticRedmineWikiExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        $configuration = new Configuration();
        $config        = $this->processConfiguration($configuration, $configs);

        $definition = $container->getDefinition('mantic_redmine_wiki.redmine_service');

        $definition->replaceArgument(1, $config['view']);
        $definition->replaceArgument(2, $config['entry_point']);
        $definition->replaceArgument(3, $config['api_key']);
        $definition->replaceArgument(4, $config['project_url']);
        $definition->replaceArgument(5, $config['wiki_url']);
        $definition->replaceArgument(6, $config['attachments_path']);

        $container->setAlias(RedmineService::class, 'mantic_redmine_wiki.redmine_service');

    }
}
