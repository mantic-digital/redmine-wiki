# Configuration needed 

You will need to create some files in order for the bundle to work.

## View

You will have to create a view to display the following values:

- page:
  - title: (string) The title of your wiki page
  - content: (html) The html content of your wiki page (without the title)
- subtitle: (string)
- breadcrumb: (array of)
  - title: (string) The title of your wiki page
  - link: (string) The href to that page within this bundle
- attachments: (array of)
  - id: (int) The attachment id
  - name: (string) The attachment name
  - desc: (string) The attachment description
  - type: (string) The attachment type (used for generate icon)

## Configs

Create a Yaml file under the config/packages directory and add the following content:

    # config/packages/mantic.yaml
    # let's say your wiki start page is located at: https://your.redmine.com/projects/your_project/wiki/wiki_start_page
    mantic_redmine_wiki:  
      view: // The view you created in your templates folder
      entry_point: 'wiki_start_page'  
      api_key: // you will find your API key in your Redmine account (you probably should create a readonly user)
      project_url: 'https://your.redmine.com'  
      wiki_url: '/projects/your_project/wiki/'  
      attachments_path: 'attachments/'

Then insert your wiki details on the file you've just created.

## Routes

Go to you project routes Yaml file and add the following:

    mantic_redmine_wiki_routes: //this can be the name you want to
      resource: "@ManticRedmineWikiBundle/Resources/config/routing.yml"
      prefix: /your_prefix/

Or if you want to create your own routes copy the contents of the file [routing.yml](Resources/config/routing.yml) under Resources/config. 
If you choose this option take in account that you must not change the routes names.

## Attachment icons

The attachments icons make use of Font Awesome.